  * fix implement systemctl wrapper supporting both d-i and shell use
  * use config snippet for class Service.time.chrony.pre-resolve
  * Auto-populate l10n classes
    * Skip entries with subclasses (assuming custom override)
  * Add Game class
  * Add packages hunspell-mn hyphen-mn since bookworm
  * Add package wdq since bookworm
  * Add documentation subclasses e.g. for LibreOffice and Gimp.
  * Figure out if possible to use variables for per-node override:
    * gui-toolkit: gtk/qt
    * desktop-environment: xfce/lxde/gnome/kde
    * locale-en: british/american
    * locale-hunspell-da: myspell/hunspell
    * db: sqlite/mysql/postgres
  * Add READMEs in (sub)classes to document naming conventions etc.
  * Add localegroup NORDIC and fix shrink SCANDINAVIA.
  * Add l10n.all and include in appropriate ...locale.ALL classes.
  * Rename ...locale.ALL → ...locale.WORLD.
  * Implement CLI typewriter setup from
    <http://crunchbang.org/forums/viewtopic.php?pid=368469#p368469>
  * Integrate xfce4-whiskermenu-plugin as xfce subclass.
  * Add power management (sub)class for tlp
  * Use and configure "locales" package (or "locales-all" when suitable)
  * Add class for odd connectivity, e.g. iPhone/GPRS
    * include libusbmuxd-tools
    * include comgt
  * Add class for odd networking protocols
    * include gvfs-backends
  * Test script.sh of each node with shellcheck and "dash -n"
  * Test that all MTAs are avoided for Service.mail.mta.avoid.
  * Register and track packages with limited security support.
  * Add class for virtual keyboards
    * onboard
    * kvkbd
    * florence
    * matchbox-keyboard
      * matchbox-keyboard-im
    * qtvirtualkeyboard-plugin
  * Maybe include dex in lightweight desktop class.
  * Include xssproxy in simple desktop classes.
  * Put Xfce to sleep when closing lid, both on battery and powered.
  * Add classes for console-based calendaring/tasks/addresses:
    * remind + wyrd/tkremind
    * taskwarrior + ptask/taskd/vit/tasksh/timewarrior
    * calcurse
    * khal
    * khard
    * vdirsyncer
    * todotxt-cli
  * Add class for console-based audio processing:
    * cava
  * Add class for console-based image viewing:
    * catimg
  * Add more font design tools - e.g. UFO-related ones
    * Include package fonts-inter since Bookworm
    * Include package trufont
    * Include package libharfbuzz-bin since Bookworm
      (for web optimizing tool hb-subset provided since version 2.9.0)
  * Add Desktops:
    * Budgie desktop
    * Cinnamon desktop
    * Deepin desktop
    * GNOME desktop
    * KDE desktop
    * LXDE desktop
    * Matchbox desktop
    * MATE desktop
    * Plasma desktop
    * Xfce desktop
    * UKUI desktop
    * Lisp (using elpa-exwm)
  * Add package pinentry-fltk
  * Maybe add package ddupdate
  * Extend design class:
    * Add package plakativ
    * maybe add package color-picker since bookworm
  * Add class for low-memory machines to disable webkit2gtk gigacage anti-feature
  * Extend minimal desktop classes:
    * Add package playerctl
  * Add presentation tools
    * CLI: patat
  * Deprecate class Desktop.tools
  * Extend class Desktop.chat with additional subclasses:
    * quassel
    * kde (equivalent of metapackage kde-telepathy)
    * swift (package swift-im)
    * gtk
    * gnome
    * kde
    * qt
  * Maybe use perl Locale::Codes or ISO::639_1 to process+validate country data?
  * Add tinker class: Add packages ngspice oregano
  * Add needrestart subclass to force service reload (see bug#894444).
  * Extend class Admin.apt.auto.upgrade: Add package rover
  * Extend class Console:
    * Add package physlock.
    * Add package ranger
    * Maybe add package bmon.
  * Add class Hardware.realtime: Add packages rtirq-init linux-image-*-rt.
  * Add packages providing virtual va-driver to appropriate classes.
  * Add package logswan to some appropriate server class

  * Support enabling eatmydata to preseeding file.
    <http://people.skolelinux.org/pere/blog/Quicker_Debian_installations_using_eatmydata.html>

  * Add package xfce4-screensaver
  * Maybe include package recap with some headless service class
  * Include package rng-tools-debian for appropriate hardware classes
  * Maybe include package wpagui with some Qt network class.
  * Maybe include package ceni with some TUI network class
  * Maybe include package kylin-nm with some Qt (or only UKUI?) network class
  * Add package tig in some Command-line development class.
  * Add package process-viewer in some GUI admin class
  * Maybe include package ucarp in some network class
  * Maybe include package netctl in some Console network class
  * Maybe include package tuned in some admin class
  * Maybe include package kthresher with unattended-upgrades
  * Maybe include packages catimg chafa in some Console class.
  * Maybe include package wlogout in some Wayland class.
  * Maybe include packages img2pdf svgtune in some Console and/or design class.
  * Maybe include package mugshot in some Desktop utils class.
  * Maybe include package vorta in some Desktop utils class.
  * Maybe add class to impose rate-limiting:
    <https://gwolf.org/2021/06/fighting-spam-on-roundcube-with-modsecurity.html>

  * Add theme classes including murrine and murrine/greybird.
    * Mention bug#748971 (closed but not reliably solved).

  * Extend class Desktop to streamline theming across widget sets:
    * add packages gnome-themes-extra adwaita-qt
    * apply tweaks as documented at <https://wiki.archlinux.org/index.php/Uniform_Look_for_QT_and_GTK_Applications>
    * Add package gtk-chtheme somewhere related to gtk2
    * Maybe tweak bash profile to add "export QT_AUTO_SCREEN_SCALE_FACTOR=1"
      <https://unix.stackexchange.com/a/443531>

  * Classify http reverse proxies
    * haproxy
    * trafficserver
    * nghttp2-proxy
    * perlbal
    * pushpin
    * pound

  * Classify TLS proxies
    * slt
    * stunnel4

  * Classify http tunneling proxies
    * connect-proxy
    * corkscrew
    * desproxy
    * pagekite

  * Classify DNS proxies
    * dnsmasq
    * dnsproxy
    * dnsdist
