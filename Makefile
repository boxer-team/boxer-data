suites = bookworm trixie
getnodes = $(basename $(notdir $(wildcard $1/nodes/*.yml)))

all: symlinkfarm

symlinkfarm:
	shtool mkshadow bookworm trixie 2>/dev/null
	touch $@

test: symlinkfarm
	set -e; $(foreach suite,$(suites),\
	boxer compose --datadir $(suite) $(call getnodes,$(suite));)
	touch $@

clean:
	find * -type l -delete
	find * -empty -delete
	rm -f symlinkfarm test preseed.cfg script.sh

.PHONY: all clean
