# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Unreleased]

## [10.10.0] - 2024-08-18

### Removed

  * drop suite bullseye
  * drop class l10n.hunspell.fr.modern: package hunspell-fr-modern is gone
  * drop class l10n.hunspell.it.myspell: package myspell-fr is gone
  * drop class l10n.hunspell.kmr.myspell: package myspell-ku is gone
  * drop class l10n.hunspell.sl.myspell: package myspell-sl is gone
  * drop class l10n.ispell.uk: package iukranian is gone (or maybe bogus)

### Fixed

  * update class Desktop.web.firefox.harden: stop install webext-https-everywhere (gone)
  * fix class Framework.theme.gnome: install/avoid gnome-themes-extra (not gnome-themes-standard)
  * fix class Framework.theme.sugar: stop install/avoid gtk2-engines-sugar (gone)
  * fix class l10n.mythes.hu: install mythes-hu (not mythes-ca)
  * fix class l10n.manpages.ALL: include l10n.manpages.EUROPE (not l10n.ispell.EUROPE)
  * fix class l10n.manpages.EUROPE: include l10n.manpages.EU (not l10n.ispell.EU)
  * fix class l10n.wordlist.ALL: include l10n.wordlist.* (not l10n.ispell.*)

### Added

  * add suite trixie
  * add classes Desktop.email.locale.es Desktop.email.thunderbird.locale.es
  * add class Service.dns.llmnr
  * add classes l10n.hyphen.eo l10n.hyphen.th
  * add class l10n.ispell.it
  * add class l10n.mythes.eo
  * add class l10n.wordlist.eo
  * add class l10n.wordlist.gd
  * add class l10n.wordlist.ga
  * add class l10n.wordlist.gv
  * add class Desktop.office.libreoffice.locale.hy
  * add classes Desktop.email.locale.pa Desktop.email.thunderbird.locale.pa

### Changed

  * update classes Desktop.email.locale Desktop.email.thunderbird.locale: cover es-AR and es-MX
  * update class Service.dns: avoid LLMNR by default
  * update class Framework.pkg.apt.source.reset: update security base URI
  * update class l10n.hunspell.eo: install hunspell-eo (not myspell-eo) since trixie
  * update class Desktop.office.libreoffice.locale.INDIA: stop install libreoffice-l10n-in (metapackage)
  * update class Desktop.web.firefox.locale.AMERICAS: install firefox-l10n-trs
  * update class Desktop.web.firefox.locale.ASIA: install firefox-l10n-tg firefox-esr-l10n-tl
  * update class Desktop.web.firefox.locale.EUROPE: install firefox-esr-l10n-ca-valencia firefox-esr-l10n-fur firefox-esr-l10n-sc firefox-esr-l10n-sco firefox-esr-l10n-szl
  * update class Desktop.email.thunderbird.locale.af: install thunderbird-l10n-af
  * update class Desktop.email.thunderbird.locale.AMERICAS: install thunderbird-l10n-cak
  * update class Desktop.email.thunderbird.locale.ASIA: install thunderbird-l10n-ka thunderbird-l10n-th thunderbird-l10n-uz
  * update class Desktop.email.thunderbird.locale.EU: install thunderbird-l10n-lv
  * update class Desktop.email.thunderbird.locale.EUROPE: install thunderbird-l10n-ka
  * update class Desktop.email.thunderbird.locale.en.CA: install thunderbird-l10n-en-ca

## [10.9.12] - 2022-08-07

### Fixed

  * update class Desktop.web.webkit: have subclass gtk install surf (not luakit) since bookworm; include gnome by default (not gtk: luakit broken with Wayland, see bug#1012739)

### Added

  * add class l10n.mythes.pt.BR since bookworm
  * add class l10n.hyphen.be since bookworm

## [10.9.11] - 2022-05-31

### Fixed

  * fix class Service.ssh.openssh.reset to avoid broken symlinks by using systemctl

## [10.9.10] - 2022-05-31

### Fixed

  * fix and simplify class Service.ssh.openssh.reset to not hang after generating keys and not create and use external script

## [10.9.9] - 2022-05-28

### Fixed

  * fix class Service.time.chrony.pre-resolve: include class Admin.etc.functions

## [10.9.8] - 2022-02-17

### Removed

  * drop class Hardware.net.bufferbloat.avoid since Bookworm (fq_codel enabled by default since linux-image 5.16~rc8-1~exp1)

### Added

  * update class Desktop.office: add subclass suckless

### Documentation

  * reformat CHANGES to Keep a Changelog style
  * rename CHANGES -> CHANGELOG.md
  * rename TODO -> TODO.md
  * Consistently use asterisk as bullets in TODO

## [10.9.7] - 2022-02-03

### Changed

  * update class Desktop.design.animation: stop install package synfigstudio since bookworm

## [10.9.6] - 2022-02-03

### Added

  * update class Desktop.editor.text: add subclasses xfce code.xfce, both installing mousepad (same as gtk, for now)
  * add classes l10n.mn l10n.hunspell.mn l10n.hyphen.mn

### Changed

  * update class Desktop.editor.text.code.gtk: install package l3afpad (not mousepad) since bookworm
  * update class Service.time: use chrony by default (more accurate and smoothly adjusting than systemd-timesyncd)
  * update class l10n.hunspell.en.ZA: install package hunspell-en-za (not hunspell-en-gb)
  * update class Desktop.office.libreoffice.locale: update subclass kn to install package libreoffice-l10n-kn; update sublass EUROPE to install package libreoffice-l10n-szl
  * update class Desktop.email.thunderbird.locale: update subclass ASIA to stop install package thunderbird-l10n-si

## [10.9.5] - 2021-11-16

### Removed

  * drop obsolete empty class Service.time.systemd.pre-resolve

### Deprecated

  * empty and deprecate class l10n.ispell.it (unmaintained - see bug#996361 - and gone upstream)

### Changed

  * update class Console.mobile: install packages syncevolution syncevolution-http
  * update class Service.mail.mta.avoid: stop avoid gone package qmail-run

## [10.9.4] - 2021-11-09

### Changed

  * update class Desktop.scheduling.lightning: stop install package lightning (gone)

## [10.9.3] - 2021-10-18

### Changed

  * update class Desktop.web.webkit.gtk: install luakit (not midori: Electron-based nowadays - see bug#995398)

## [10.9.2] - 2021-09-14

### Added

  * add class Service.time.chrony.systemd to integrate chrony with systemd

### Changed

  * update class Service.filesystem.indexer to install package plocate (not mlocate); see bug#989517
  * update class Desktop.mendia.video.mpv.gtk to install celluloid (not gnome-mpv now a transitional package)
  * update class Service.class.time.chrony.pre-resolve to save tweak as a snippet

## [10.9.1] - 2021-08-19

### Fixed

  * fix create symlink farm for bookworm

### Removed

  * drop class Service.time.systemd.pre-resolve: obsolete (DNSSEC is bypassed when resolving NTP hosts since systemd 247.3-2)

## [10.9.0] - 2021-08-18

### Removed

  * drop suite buster

### Changed

  * update class Desktop.design.painting.gtk: revert to install package gimp-plugin-registry: in good shape again now

## [10.8.28] - 2021-04-23

### Fixed

  * fix have autopkgtests explicitly depend on make
  * fix exclude node showmebox-gnustep in autopkgtests on archs without thunderbird

## [10.8.27] - 2021-04-22

### Fixed

  * fix update class Service.web.uwsgi.python: install uwsgi-plugin-python3 (not uwsgi-plugin-python) since bullseye
  * fix exclude node dharma in autopkgtests on archs without thunderbird

## [10.8.26] - 2021-04-21

### Fixed

  * fix really really skip autopkgtests involving thunderbird/falkon where unavailable
  * fix drop class Service.web.wiki.moinmoin since bullseye: MoinMoin is virtually dead upstream and no longer in Debian

## [10.8.25] - 2021-04-20

### Fixed

  * fix really skip autopkgtests involving thunderbird/falkon where unavailable
  * fix class Service.mail.list: install Mailman3 and Hyperkitty (not mailman) since bullseye

## [10.8.24] - 2021-04-19

### Fixed

  * fix skip autopkgtests involving thunderbird/falkon where unavailable, and enable other arch-sepcific autopkgtests

## [10.8.23] - 2021-04-18

### Fixed

  * have autopkgtest skip arch-specific nodes, and include showmebox-gnustep (not sure why that was skipped previously); closes: bug#987079, thanks to Paul Gevers

## [10.8.22] - 2021-04-17

### Fixed

  * fix class Service.mail.avoid: stop avoid citadel-mta unavailable since bullseye; closes: bug#986726, thanks to Graham Inggs

## [10.8.21] - 2021-03-02

### Changed

  * update class Desktop.design.web.sass: stop install packages for obsolete libraries singularitygs slickmap susy yui
  * update class Desktop.design.web.sass: stop mention Compass in description
  * update class Desktop.design.web.sass: install packages for Sass libraries gutenberg sass-extras typey
  * update class Desktop.design.web.sass: stop auto-marking packages ruby-compass ruby-sass
  * update class Desktop.design.web.sass: install package sassc

## [10.8.20] - 2021-02-06

### Removed

  * drop classes Desktop.environment.xfce.scheduling Desktop.environment.scheduling.lxqt since bullseye (package orage is gone, see bug#977628)

## [10.8.19] - 2021-02-06

### Removed

  * drop classes l10n.se l10n.hunspell.se since bullseye (see bug#978987)

### Changed

  * update class Desktop.environment.lxqt.limit: stop avoid package disk-manager since bullseye (gone: see bug#943969)

## [10.8.18] - 2020-12-29

### Fixed

  * fix class Hardware.net.bufferbloat.avoid to use correct syntax

### Changed

  * update class Desktop.design.web: stop include Desktop.web.blink (see bug#972134)

## [10.8.17] - 2020-11-14

### Fixed

  * update class l10n.aspell: fix install package aspell-cs (not bogus aspell-cz) in subclass cs
  * update class l10n.hunspell: fix have subclass en.ZA include en.GB (not virtual package hunspell-en-za)
  * update class l10n.hunspell: fix drop obsolete subclasses ca.myspell de.AT.myspell de.CH.myspell de.DE.myspell gl.rag hr.myspell nl.myspell pl.myspell pt.BR.myspell pt.PT.myspell ru.myspell sv.dsso
  * fix add base class l10n.hyphen.pt
  * update class l10n.mythes: fix install package mythes-de-ch (not mythes-de) in subclass de.CH

### Added

  * add classes l10n.id l10n.hyphen.id l10n.tr l10n.hunspell.tr
  * add classes l10n.lv l10n.hyphen.lv
  * add class l10n.mythes.cs
  * add classes l10n.gug l10n.mythes.gug
  * add class l10n.mythes.id

### Deprecated

  * update class Desktop.email.thunderbird.harden: no-op since bullseye: enigmail is gone, and Thunderbird supports PGP now

### Changed

  * use POSIX sed option -E (not -r)
  * update class Desktop.web.firefox.harden: install package webext-ublock-origin-firefox (not webext-ublock-origin) since bullseye
  * update class Desktop.email.thunderbird.locales: install package thunderbird-l10n-kab to subclass AFRICA
  * update class Desktop.email.thunderbird.locales: install packages thunderbird-l10n-dsb thunderbird-l10n-hsb to subclass EUROPA
  * update class l10n.hunspell: consistently provide explicit hunspell subclass when both hunspell and myspell variants exist
  * update class l10n.hunspell: consistently avoid explicit hunspell subclass when the only exisitng variant
  * update class l10n.hunspell: consistently provide explicit myspell subclass (even if the only exisitng variant)
  * update class l10n.ispell: stop include subclass de.DE.old in de.DE
  * update class l10n.wordlist: stop include subclass de.DE.old in de.DE
  * update class l10n: tighten localeregion classes

### Documentation

  * spellcheck changelog

## [10.8.16] - 2020-08-18

### Fixed

  * fix mark many auto-installed packages since Buster (not only Stretch), as intended since 10.8.12

### Removed

  * drop suite stretch

### Added

  * add suite bookworm

### Changed

  * update class Desktop.design.color.gtk: install package gcolor3 since bullseye; stop mention gcolor2 for buster
  * update class Desktop.web.firefox.harden: stop install package webext-noscript (arguably superseded by webext-ublock-origin)
  * update class Service.time: have base class include Service.time.systemd (not Service.time.openntpd)
  * update class Desktop.design.painting.gtk: avoid package gimp-plugin-registry since bullseye: in too bad shape

## [10.8.15] - 2020-07-07

### Removed

  * update class Admin.apt.tools: drop subclass libreload.whatmaps: deprecated (see bug#964470#10)
  * drop class Framework.media.wildmidi.limit since buster: bug#612509 fixed
  * drop class Service.dns.systemd.fallback.avoid since buster: bug#923081 fixed

### Added

  * add class Hardware.motherboard.olimex.a64 since Buster

### Changed

  * update classes Hardware.laptop Hardware.motherboard: stop include class hw.firmware for ARM systems
  * update class hw.firmware: include firmware-ath9k-htc since Buster, but temporarily avoid since Bullseye (bug#944169, #951891)
  * update class Framework.pkg.apt.source.reset: omit security when no VERSION in /etc/os-release; omit *-updates when targeted sid
  * update class Admin.apt.tools: add subclass support, and include in base class

### Documentation

  * drop obsolete bug hints

## [10.8.14] - 2020-06-29

### Deprecated

  * update class Desktop.office.libreoffice: make subclass theme.tango a noop since Buster (package libreoffice-style-tango no longer optional since then)

## [10.8.13] - 2020-06-12

### Removed

  * drop class Hardware.gl.software

### Added

  * update class Hardware.nic: add subclass broadcom.brcm: include firmware-brcm80211
  * update class hw: add subclass nic
  * update class Hardware.nic: add subclass intel
  * update class Hardware.nic: add subclass prism2
  * update class Hardware.nic: add default classes, omitting packages needing network or compilation during install
  * update class Console.design: add subclasses font web
  * add class Task.text

### Changed

  * update class Desktop.design.pdf: include pdfarranger since Buster
  * update class Desktop.design.pdf: mention BookletImposer
  * update class Desktop.design.pdf: stop include origami-pdf since Bullseye
  * update class Hardware.nic: extend subclass atheros: include firmware-ath9k-htc
  * update class Hardware.nic: extend subclass atheros: nonfree-include firmware-zd1211
  * update class Desktop.web.webkit.gtk: add midori (not surf)
  * update class Console.design: update subclass painting.gtk: add drawing mypaint mypaint-data-extras since bullseye, stop mention mypaint before bullseye
  * update class Framework.crypto.gnupg.pinentry.qt: include pinentry-qt (not pinentry-qt4)

## [10.8.12] - 2020-01-26

### Changed

  * update class Desktop.scheduling.lightning: install package webext-dav4tbsync since buster
  * mark many auto-installed packages
  * update class Desktop.tools.account: install package usermode (not gnome-system-tools); see bug#888675,#888670

## [10.8.11] - 2019-12-01

### Added

  * Add class Hardware.net.bufferbloat.avoid since Buster.
  * Add class Console.locale.default.reset.
  * Add class Console.setup.root.reset.
  * Extend class Console.filemanager: Add subclass mc.full, enabled by default.

### Changed

  * Update class Desktop.office.libreoffice: Have subclass gnome stop add gtk. Have subclass gtk add gtk.gtk3 (not gtk.gtk2) since Bullseye.
  * Fix class Hardware.box.globalscale: Include class Hardware.bootloader.u-boot.
  * Update class Console.design.drawing: Stop install package usvg (too immature).
  * fix class Desktop.media.audio.alsa: Include volumeicon-alsa (not volti); see bug#943985

### Packaging

  * declare compliance with Debian Policy 4.4.1
  * bump debhelper compatibility level to 9

## [10.8.10] - 2019-08-31

### Fixed

  * Fix class Service.init.sysv: Include/exclude package sysvinit-core (not obsolete sysvinit).
  * Fix classes Hardware.net.interfaces.dhcp.systemd Service.firewall.systemd: Broken syntax in tweak.

### Added

  * Update class Service.dns: Add subclasses bind unbound systemd resolver.

### Changed

  * Update class Service.dns: Use caching service (not authoritative) by default. Use systemd (not unbound) as default caching service.

## [10.8.9] - 2019-08-28

### Changed

  * Update class Framework.theme.xfce: Include subclass gtk3 (not gtk2) and drop subclass gtk2 since Bullseye. See bug#935954.

## [10.8.8] - 2019-08-27

### Fixed

  * Fix class Service.supplicant.iwd: Broken syntax.

## [10.8.7] - 2019-08-26

### Fixed

  * Fix class Service.init.systemd description.
  * Fix classes Hardware.net.enable.systemd Hardware.net.interfaces.dhcp.network-manager Hardware.net.interfaces.dhcp.systemd Service.firewall.firewalld.network-manager Service.firewall.systemd, and most of class Hardware.net.interfaces.functions (broken since introduced in release 10.8.1).

### Added

  * Add class Service.ssh.openssh.reset.
  * Add class Service.init.systemd.reset.
  * Add class Service.ssh.advertise.
  * Update class Service.ntp: Add subclasses chrony systemd.
  * Update class Service.ssh: Add subclass mosh.
  * Add class Service.supplicant.
  * Add classes Hardware.box.globalscale Hardware.motherboard.olimex.micro hw.soc.marvell.kirkwood.

### Changed

  * Update class Admin.tools.network.wifi: Include (also since Buster) package iw (was removed since Buster in release 10.7.3).
  * Update class Service.init: Include class Service.init.systemd.default by default (not Service.init.sysv.default).
  * Update classes Desktop.mobile.sync.gtk Console.mobile.sync: Stop include syncevolution sync-ui since Bullseye (see bug#935239).

### Documentation

  * Update TODOs.

## [10.8.6] - 2019-08-24

### Fixed

  * Update class Desktop.web.webkit.qt: Install package qutebrowser-qtwebkit (not aora) since Buster. Closes: Bug#935608. Thanks to Paul Gevers.

### Removed

  * Update class Desktop.web.firefox.locale.bn: Drop subclasses BD IN, and always install package firefox-esr-l10n-bn (not firefox-esr-l10n-bn-bd firefox-esr-l10n-bn-in).
  * Update class Desktop.web.firefox.locale: Drop subclass or.

### Changed

  * Update class Desktop.web.firefox.locale.en.CA: Install package firefox-esr-l10n-en-ca.
  * Update class Desktop.web.firefox.locale.en.ZA: Stop install package firefox-esr-l10n-en-za.
  * Update class Desktop.web.firefox.locale.INDIA: Stop install package firefox-esr-l10n-mai.
  * Update class Desktop.web.firefox.locale.ml: Stop install package firefox-esr-l10n-ml.
  * Update class Desktop.office.libreoffice.locale.EUROPE: Install package libreoffice-l10n-gd.

## [10.8.5] - 2019-08-16

### Fixed

  * Update class Admin.tools.network.wifi: Fix ensure initial start of WiFi daemon iwd on Buster (see bug#934194).

### Added

  * Extend class Framework.rdf: Add subclass easyrdf since Bullseye.

### Changed

  * Update class Console.design.drawing: install package cairosvg (not python-cairosvg) since Bullseye (see bug#934911). install package usvg since Bullseye.

### Documentation

  * Update TODOs.

## [10.8.4] - 2019-07-26

### Fixed

  * Fix class Admin.etc.functions: Merge consecutive redirects.

## [10.8.3] - 2019-07-26

### Fixed

  * Fix class Admin.etc.functions: Single-quote regular expressions.
  * Fix class Admin.etc.functions: Broken syntax in function _backup().

## [10.8.2] - 2019-07-25

### Fixed

  * Update class Desktop.office.libreoffice.presentation: Stop install package libreoffice-ogltrans. Closes: Bug#931032. Thanks to Rene Engelhard.

### Added

  * Introduce suite bullseye.

### Testing

  * Update autopkgtest to cover bullseye (not buster).

### Changed

  * Update class Task.music.notation.musescore: install package musescore3 (not musescore) since Bullseye.

## [10.8.1] - 2019-07-08

### Added

  * Update class Admin.etc.functions: Add function _uuid().
  * Update class Hardware.net.interfaces.functions: Add functions _iface_set_sociable_nm() _iface_set_private_nm() _iface_create_nm().
  * Add classes Hardware.net.enable.systemd Hardware.net.interfaces.dhcp.network-manager Hardware.net.interfaces.dhcp.systemd Service.firewall.firewalld.network-manager Service.firewall.systemd.
  * Update class Admin.etc.functions: Add function _clearinikey().

### Changed

  * Update class hw.smp: Stop install package irqbalance.
  * Fix class hw.arm.kirkwood: install package linux-image-marvell (not obsolete linux-image-kirkwood).
  * Tighten class Admin.etc.functions: Skip check for existence before grep.
  * Tighten class Admin.etc.functions: More compact backup function.
  * Update class Admin.apt.auto.core: Mark more packages auto-installed: e2fsprogs kmod.

### Documentation

  * Update TODOs.

## [10.8.0] - 2019-05-03

### Fixed

  * Update class Framework.pkg: Fix drop subclasses apt.aptdaemon apt.cli.aptdaemon apt.gtk.aptdaemon: Obsolete.
  * Update class Framework.pkg: Fix drop subclasses packagekit.gtk gtk.packagekit: Obsolete.
  * Fix class Hardware.laptop.olimex: Provide only since Buster. Include class Hardware.bootloader.u-boot.sunxi.
  * Fix classes Desktop.email Desktop.email.harden: Thunderbird is available in stable Debian again.
  * Update class Desktop: Fix drop obsolete subclass environment.razorqt. Add subclasses environment.lxqt environment.qt tools.qt tools.network.qt media.qt media.video.qt media.video.mpv.qt office.qt office.libreoffice.base photo.view.qt qt screensaver.suckless terminal.qt web.qt. Add classes x11.sddm.elarun Framework.crypto.openssh.pinentry.qt. Update node showmebox-qt to use LXQt (Not Razor-Qt).
  * Fix class Desktop.environment.mate: Avoid metapackages. Add subclass image-viewer.

### Added

  * Add class Framework.pkg.apt.muon.
  * Update class Framework.pkg: Add subclasses gnome kde.
  * Update class Desktop: Add subclasses environment.lxqt environment.qt tools.qt tools.network.qt media.qt media.video.qt media.video.mpv.qt office.qt office.libreoffice.base photo.view.qt qt screensaver.suckless terminal.qt web.qt.
  * Update class Desktop: Add classes x11.sddm.elarun Framework.crypto.openssh.pinentry.qt.

### Changed

  * Update class Framework.vcs.mr: Stop mark mr auto-installed.
  * Improve class Admin.etc.functions: Introduce functions _setinivar _setaddinivar.
  * Improve class Admin.etc.functions: Avoid _backup to .bak same day as .orig.
  * Improve class Admin.etc.functions: Favor _setvar on already enabled keyword over uncommenting.
  * Improve class Admin.etc.functions: Cover _setvar with space around = sign.
  * Improve class Admin.etc.functions: Favor _setline on already enabled keyword over uncommenting. Simplify _setvar _setinivar.
  * Update class Desktop.environment.gnustep.scheduling: install package addressmanager.app.
  * Update class Desktop: Update node showmebox-qt to use LXQt (Not Razor-Qt).

### Documentation

  * Update TODOs.

## [10.7.7] - 2019-04-02

### Changed

  * Fix class Desktop.environment.xfce: Explicitly install package xcfe4-panel.

## [10.7.6] - 2019-03-12

### Changed

  * Fix use hint "bug" (not bogus "bugs" unsupported since boxer 1.3.0). Closes: Bug#924382. Thanks to Santiago Vila <sanvila@debian.org>.

## [10.7.5] - 2019-02-18

### Changed

  * Fix autopkgtest: Depends on boxer. Closes: Bug#919581. Thanks to Paul Gevers.
  * Update class Admin.harden: install package debian-security-support. install package hardening-runtime since Buster.
  * Update class Framework.rdf.redland: install package python3-librdf (not python-librdf) in subclass binding.python. install package python-rdflib-tools in subclass tools. Add subclass binding.r since Buster. install package ontospy in subclass tools since Buster.
  * Update TODOs.

## [10.7.4] - 2019-01-15

### Fixed

  * Fix class Framework.media.gstreamer: Stop reference obsolete v0.10 libraries.
  * Fix class Service.mail.sieve: install package dovecot-managesieved (not bogus dovecot-managesieve).
  * Fix class Admin.harden: Stop include obsolete package harden.
  * Fix class cli.compression.uncommon: Stop include obsolete package zoo.
  * Fix provide data only for suites Stretch and Buster (not also future Bullseye).

### Removed

  * update class Desktop.editor.text: drop subclasses kephra kwrite leafpad mousepad pyroom
  * update class Framework.notebook: drop obsolete subclass ipython
  * drop obsolete classes Desktop.web.java Desktop.web.firefox.java
  * update class Language.java: drop obsolete subclasses openjdk6 openjdk7

### Added

  * update class Desktop.editor.text: add subclasses code prose gtk kde qt
  * update class Framework.notebook: add subclass jupyter
  * update class Language.java: add subclasses avoid limit; Add subclass openjdk11 since Buster

### Changed

  * Extend autopkgtest: Check that resolved packages are installable.

### Documentation

  * Update TODOs.

## [10.7.3] - 2018-12-28

### Added

  * Extend class Service.ssh: Add subclasses client server openssh.

### Changed

  * Update class Admin.tools.network.wifi: install package iwd (not iw wireless-tools rfkill) since Buster. install package wpasupplicant before Buster.
  * Update class Service.ssh: Stop mark as auto-installed packages openssh-blacklist openssh-blacklist-extra: Obsolete.
  * Update class cli.ssh-server: Stop install package molly-guard: Too unreliable.
  * Update class Hardware.laptop.olimex.teres1: Provide since Stretch (not Buster): Simplifies use even if it requires backported packages.

## [10.7.2] - 2018-12-27

### Removed

  * Update class Desktop.design.font.tools.gtk: Stop include typecatcher since Buster.

### Packaging

  * Declare compliance with Debian Policy 4.3.0.

## [10.7.1] - 2018-12-15

### Removed

  * Drop class Desktop.media.video.web: Unused since Jessie.
  * Drop class Desktop.web.firefox.locale.zu Desktop.web.locale.zu l10n.hunspell.de.AT.hunspell l10n.hunspell.de.CH.hunspell l10n.hunspell.de.DE.hunspell l10n.hunspell.en.GB.myspell l10n.hunspell.en.ZA.myspell l10n.hunspell.lt.myspell: Unused since Jessie.

### Changed

  * Update class Desktop.filesystem.indexer.locate to use subclass gtk (i.e. package catfish, not subclass xfce i.e. package xfce4-linelight-plugin) since Buster.
  * Update class Desktop.environment.xfce.file-manager: Stop explicitly install package gvfs (handled in package thunar since long).
  * Update class Desktop.environment.xfce: Include (not avoid) subclass audio (GStreamer 0.10 is gone).
  * Update class Desktop.mobile.sync.gtk: Stop flag package evolution-data-server as auto-installed.
  * Update node huayruro-classmate: Include class Desktop.web.webkit (not Desktop.web.webkit.gnome): package midori updated to use modern WebKitGTK+.
  * Update class Desktop.design.pdf: Include bookletimposer. Stop include pdfshuffler since Buster.

## [10.7.0] - 2018-11-12

### Removed

  * Stop provide deprecated suite extensions.
  * Stop provide obsolete suites wheezy jessie.
  * Drop classes Desktop.scheduling.locale Desktop.scheduling.lightning.locale (obsolete since Quantum release of Thunderbird).

### Added

  * Introduce future suite bullseye.

### Changed


### Packaging

  * Simplify rules: Use short-form dh sequencer (not cdbs). Stop build-depend on cdbs.
  * Declare compliance with Debian Policy 4.2.1.
  * Update copyright info: Extend coverage for main upstream author.
  * Update copyright info: Fix update source URL.

## [10.6.8] - 2018-11-12

### Removed

  * Fix class Desktop.email.thunderbird.locale: Drop locale subclasses bn pa ta (unavailable since Quantum release).

## [10.6.7] - 2018-11-10

### Fixed

  * Fix class Desktop.email.thunderbird.bidi: install package thunderbird-bidiui (not iceweasel-bidiui) since Buster. See bug#913391.

## [10.6.6] - 2018-11-10

### Fixed

  * Update class Framework.pkg.apt.source.reset: Fix use host deb.debian.org (not httpredir.debian.org).
  * Fix class Service.web.mail.cider: Really install (not auto-install) package ciderwebmail.
  * Fix class Service.web.apache.gnutls: Really install package libapache2-mod-gnutls.
  * Fix class Service.web.wiki.moinmoin: Really install (not auto-install) package python-moinmoin.
  * Fix class l10n.hunspell.de.medical: install package hunspell-de-med (not bogus hunspell-en-medical).
  * Fix class l10n.hunspell.de.DE: Include/avoid package myspell-de-de-1901 (not myspell-de-de-oldspell) since Stretch.
  * Fix class l10n.hunspell: Really use hunspell (not myspell) for locales en.GB en.ZA since Jessie (as intended since June 2016, git commit 834e2bf, despite reverse commit message).
  * Fix class Desktop.design.painting: Avoid packages mypaint mypaint-data-extras (see bug#894757).
  * Fix class l10n.hunspell.de.de.old: Fix install package myspell-de-de-1901 (not myspell-de-de-oldspell) since Stretch.
  * Fix class Desktop.web.firefox.harden: install packages webext-https-everywhere webext-ublock-origin. Stop install packages xul-ext-certificatepatrol xul-ext-cookie-monster xul-ext-flashblock xul-ext-noscript xul-ext-refcontrol xul-ext-requestpolicy xul-ext-y-u-no-validate (obsoleted by switch to Quantum Firefox).
  * Fix class Desktop.email.thunderbird.locale.fi: Stop install xul-ext-mozvoikko (handled by thunderbird-l10n-fi, see bug#792367).

### Removed

  * Drop class l10n.hunspell.bg.myspell: Obsolete since Stretch (and now implied until then).
  * Drop class l10n.hunspell.sv.myspell: Transitional since (at least) Jessie.

### Added

  * Update class l10n.hunspell: Add subclasses bo nb.hunspell nn.hunspell since Stretch. Add subclasses dz gug id since Buster.

### Deprecated

  * Deprecate class Hardware.net.pac.avoid: Obsolete since libproxy 0.4.x.
  * Deprecate class l10n.hunspell.en.US.myspell.
  * Deprecate class Desktop.email.thunderbird.sieve: Obsoleted by switch to Quantum Firefox.

### Changed

  * Update class l10n.hunspell: Use hunspell (not myspell) for locales bg ca cs el es fr he hu nl pl pt.BR pt.PT ru sk sv since Stretch. Use hunspell (not myspell) for locale lv since Buster.
  * Update class Desktop.web.iceweasel.locale: revert to install packages firefox-esr-l10n-be firefox-esr-l10n-de firefox-esr-l10n-sl firefox-esr-l10n-uk, previously dropped since Stretch.
  * Update class Desktop.web.iceweasel.locale: install packages firefox-esr-l10n-cak firefox-esr-l10n-ia firefox-esr-l10n-ka firefox-esr-l10n-kab firefox-esr-l10n-my firefox-esr-l10n-ne-np firefox-esr-l10n-oc firefox-esr-l10n-ur since Jessie.
  * Update class l10n.hunspell: Add subclass sq since Buster.
  * Update class Admin.auto.all: Flag as auto-installed package dbus since Jessie. Flag as auto-installed package apparmor since Buster.
  * Update class hw.crypto: install package jitterentropy-rngd (not haveged) since Buster.
  * Update class Console.design: install package scour (not python-scour) since Buster.
  * Extend class Desktop.web.firefox.harden: install packages webext-https-everywhere webext-ublock-origin since Buster.
  * Update class Desktop.sheduling.lightning.locale: install packages thunderbird-l10n-* (not now transitional lightning-l10n-*).
  * Extend class Desktop.email.thunderbird.locale: install packages thunderbird-l10n-cy thunderbird-l10n-kk thunderbird-l10n-ms.

### Documentation

  * Update TODOs.

## [10.6.5] - 2018-09-15

### Fixed

  * Fix (really this time) class l10n.hunspell.nl (since buster): install package hunspell-nl (not myspell-nl). Closes: Bug#886814. Thanks to Rene Engelhard.
  * Fix avoid needlessly install package aptitude from classes Framework.pkg.apt.proxy.reset Framework.pkg.apt.source.functions.

### Added

  * Add class Desktop.environment.kde. Add class x11.sddm since Stretch.
  * Extend class Desktop.web: Add subclasses blink.gtk chromium konqueror. Add subclasses blink.qt falkon qutebrowser since Buster.
  * Add class Desktop.environment.ukui since Buster.

### Changed

  * Update class Console.tools: Stop include classes Console.tools.media Framework.documentation Framework.vcs (but add explicitly - i.e. keep them - in superclass Console). Include class Console.editor.
  * Update class Console.tools: install package wget (not class Console.tools.web) in class Console.tools.network.
  * Update class Framework.pkg.apt.source.reset: Use host deb.debian.org (not httpredir.debian.org).

### Documentation

  * Add more ideas to TODO.

## [10.6.4] - 2018-07-13

### Added

  * Update class Hardware.laptop.purism.librem13v1: Add class Hardware.nic.realtek (uses firmware RTL8168E-2 available since firmware-nonfree 0.38).
  * Add class hw.soc.allwinner.a64 (since jessie). Add class Hardware.laptop.olimex.teres1 (since buster).

### Testing

  * Add autopkgtest.

### Packaging

  * Update Vcs-* fields: Source moved to Salsa.
  * Declare source package as not requiring root to build.
  * Declare compliance with Debian Policy 4.1.1.
  * Update package relations: Stop build-depend on dh-buildinfo.

### Documentation

  * Add a bunch of ideas to TODO file.

## [10.6.3] - 2018-01-21

### Fixed

  * Fix class Desktop.web.firefox.locale.de spuriously dropping all packages when included (since stretch): Single package commented out need pkg: declaration itself commented out.
  * Fix class l10n.hunspell.nl (since buster): install package hunspell-nl (not myspell-nl). Closes: Bug#886814. Thanks to Rene Engelhard.

### Added

  * Add new classes Hardware.igp.intel.skylake Hardware.laptop.purism Hardware.motherboard.intel.skylake-u Hardware.nic.atheros Hardware.sound.intel.

### Changed

  * Update class Console.design.painting: install package optipng (not inferior package pngcrush).
  * Update node huayruro-classmate: Stop include class Desktop.office.gtk.
  * Update class Desktop.office.libreoffice.word-processor: Stop avoid package libreoffice-emailmerge (since jessie): Obsolete.
  * Update classes Desktop.environment.xfce.base Desktop.environment.xfce.audio: Include/avoid package xfce4-pulseaudio-plugin (since stretch), and stop include/avoid xfce4-mixer xfce4-volumed.
  * Update class Console.media.audio.pulseaudio.client: install package pulsemixer (since buster).

### Documentation

  * Add some TODOs.

## [10.6.2] - 2017-11-13

### Fixed

  * Fix class x11.lightdm.gtk to install package slick-greeter (not bogus lightdm-slick-greeter). Thanks to Mattia Rizzolo.

### Added

  * Add class hw.soc.allwinner.a64 (since buster).

## [10.6.1] - 2017-11-09

### Fixed

  * Fix class l10n.hunspell.sl: install package hunspell-sl (not myspell-sl). Closes: Bug#872712. Thanks to Mattia Rizzolo and Rene Engelhard. Relax class Desktop.web.firefox.locale.EU (since stretch): Avoid package firefox-esr-l10n-sl (see bug#881241).

### Added

  * Extend class x11.lightdm.gtk: Add subclass slick since buster, and use that by default (gtk greeter recommends metapackages).

### Documentation

  * Update TODOs.

## [10.6.0] - 2017-11-05

### Fixed

  * Fix class Desktop.web.iceweasel.locale: Drop locale be (since stretch). Closes: Bug#864985. Thanks to Peter Green.
  * Fix class Desktop.designcolor: stop install package gcolor2 (since stretch). Closes: Bug#869155. Thanks to Tobias Frost.
  * Fix class Admin.etc.functions to use grep -E (not deprecated egrep). See bug#867658, #867695.

### Added

  * Add suite buster.

### Packaging

  * Simplify rules: Stop resolve package relations in rules file.
  * Modernize Vcs-* fields: Consistently use git (not cgit) in path. Consistently include .git suffix in path.
  * Declare compliance with Debian Policy 4.1.1.
  * Stop override lintian for package-needs-versioned-debhelper-build-depends: Fixed in lintian.
  * Tighten lintian overrides regarding License-Reference.
  * Update copyright info: Use https protocol in file format URL.
  * Update copyright info: Extend coverage for myself.

### Documentation

  * Fix typo in changelog entry.

## [10.5.20] - 2017-04-19

### Fixed

  * Fix isolate deprecated classes from non-deprecated classes

## [10.5.19] - 2017-04-19

### Fixed

  * Fix included jessie-deprecated classes, avoiding broken symlinks. Closes: Bug#860581. Thanks to Andreas Beckmann.

## [10.5.18] - 2017-04-14

### Fixed

  * Fix class Desktop.scheduling.lightning: Install package lightning (not bogus lightning-extension).

## [10.5.17] - 2017-04-14

### Fixed

  * Fix revert class l10n.hunspell to use myspell (not hunspell) for Norwegian: Firefox locale packages still too picky.
  * Fix revert class Desktop.email.thunderbird.bidi to install package icedove-bidiui (not bogus thunderbird-bidiui).

## [10.5.16] - 2017-04-14

### Fixed

  * Fix re-enable Norwegian dictionary and locale packages (see bug#824750, #824776).
  * Fix re-enable Icelandic dictionary package (see bug#782720).
  * Fix re-enable Gaelic locale package (see bug#782720).
  * Fix re-enable Lithuanian locale packages, and favor hunspell over myspell (see bug#824750, #824776, #824796).
  * Fix favor hunspell dictionary over myspell for Croatian (see bug#824750, #824776).

### Added

  * Re-enable class Console.mobile.sync (see bug#824426).

### Deprecated

  * Re-add class Desktop.web.iceweasel, but deprecated.
  * Re-add classes Desktop.email.icedove Desktop.email.locale, but deprecated.

### Changed

  * Rename class Desktop.web.iceweasel → Desktop.web.firefox.
  * Adapt classes referencing Desktop.web.iceweasel (or Desktop.web.locale).
  * Rename class Desktop.email.icedove → Desktop.email.thunderbird. Rename class Desktop.scheduling.iceowl → Desktop.scheduling.thunderbird.
  * Adapt classes referencing Desktop.email.icedove or Desktop.scheduling.iceowl (or Desktop.email.locale or Desktop.scheduling.locale).

## [10.5.15] - 2017-03-04

### Changed

  * Improve class Framework.pkg.apt.avoid-removals: Avoid removals without favoring downgrades. Add more bug references.
  * Avoid Pencil2D: Deemed immature by its package maintainer. See bug#854277.

## [10.5.14] - 2016-12-29

### Changed

  * Extend class Console: Add subclass web. Include Console.web in Console. Include Console.web.tools in Console.tools.network.
  * Extend class Console.tools.network: install package rsync.
  * Extend class Admin.apt.tools.changes: Add subclasses confirm which.
  * Change class Admin.tools: Include class Admin.tools.disk.partition. Stop include classes Console.tools.media Framework.documentation Framework.vcs Admin.tools.dns.

## [10.5.13] - 2016-12-22

### Changed

  * Avoid BookletImposer since stretch. See bug#834007 (and #846263).

## [10.5.12] - 2016-11-05

### Changed

  * Fix release version and date in CHANGES.
  * Add device-specific u-boot classes.
  * Fix class Framework.pkg.apt.source.functions (missing escape in function _setaptsrc).
  * Fix class Admin.disk.auto (append var if missing: rcS file is empty by default since stretch).
  * Have class Admin.auto stop include Admin.disk.auto. Mention in Admin.disk.auto description it being useful only with non-systemd init systems.
  * Fix class Framework.pkg.apt.source.functions (missing separator before function _resetaptsrc).
  * Fix class Framework.pkg.apt.proxy.reset (wrongly expected /etc/apt/apt.conf to always exist).
  * Fix class Framework.pkg.apt.source.reset (broken variable expansions, stray trailing curly quote).
  * Fix class Service.mail.mta.avoid for Stretch: list MTAs individually (cannot avoid a virtual package).
  * Add TODO on testing Service.mail.mta.avoid coverage.
  * Extend class Service.mail.mta: Add subclasses dma msmtp.
  * Extend class Service.firewall: Add subclass firewalld.
  * Add class Framework.localization.limit.
  * Stop include atop in class Admin.tools.processes (only limited use).
  * Extend class Admin.tools.dns: Add subclass knot. Move default to subclass bind9.
  * Fix class Service.init.systemd.init (accidentally included wrong package sysvinit).
  * Extend class Service.scheduler: Add subclass systemd.
  * Extend class Admin.apt.tools: Add subclass libreload since Jessie, included in main class.
  * Add TODO about packages with limited security support.
  * Fix class Desktop.office.libreoffice since Stretch: Add gtk subclasses gtk2 gtk3, and adjust subclass gnome to use gtk.gtk3. Closes: Bug#833796. Thanks to Rene Engelhard.

## [10.5.11] - 2016-06-18

### Changed

  * Add class Framework.pkg.apt.source.functions.
  * Add class Console.tools.media.
  * Fix class Admin.etc.functions to escape $ in regex.
  * Improve class Admin.etc.functions to bang-enclose regex (instead of comma, more likely to appear in content.
  * Improve class Admin.etc.functions: Introduce functions _setappendline _setappendvar.
  * Add class Service.init.
  * Fix temporarily suppress class Console.mobile.sync since Stretch (see bug#824426).
  * Update class Desktop.scheduling.iceowl.locale: Include locales ar be bn br cy el fi gd gl he hy id lt nb-no pa pt rm ro si sl sq sr ta tr uk vi since Jessie (not only Stretch: Introduced in point release 8.5).
  * Update class Desktop.web.iceweasel.locale: Include locales an az dsb eo gn hsb ms sr uz  since Jessie (not only Stretch: Introduced in point release 8.5).
  * Update class Desktop.web.iceweasel.locale: Fix avoid obsolete locale zu since Jessie (not only Stretch).
  * Update class Desktop.email.icedove.locale: Have locale class bn include bn.IN since Stretch.
  * Update class l10n.hunspell: Use myspell (not hunspell) for locales en.GB en.ZA since Jessie (not only Stretch).
  * Extend class Desktop.mobile.sync: Subclass by widget framework and add more packages.
  * Add classes hw.soc.ti Hardware.nic.ti Hardware.nic.marvell Hardware.motherboard.goldelico Hardware.laptop.openpandora.
  * Extend class Console.tools: Include Console.compression.
  * Extend class Console.tools: Include Console.tools.media.
  * Add class Language.c.
  * Extend class Console.tools.filesystem: install package eject.
  * Extend class Admin.tools.hardware: install package usbutils.
  * Extend class Service.display.xorg.video: Add subclasses all.avoid omap.
  * Extend class Admin.tools: Add subclasses disk hardware.low-level network.

## [10.5.10] - 2016-05-31

### Changed

  * Fix class Desktop.web.iceweasel.locale: Avoid locales no-* in EUROPE (not only SCANDINAVIA).
  * Fix class Desktop.web.iceweasel.locale: Avoid including metapackage icedove-l10n-all.
  * Fix class Desktop.email.icedove.locale: Avoid locales no-* in EUROPE (not only SCANDINAVIA).
  * Improve class Desktop.scheduling.iceowl.locale: Stop avoid locales lt no-* (see bug#824750).
  * Improve class Desktop.web.iceweasel.locale: Stop avoid locale bg (not affected my myspell/hunspell issue).
  * Improve class Desktop.web.iceweasel.locale: Include locale pa-in.
  * Improve class Desktop.office.libreoffice.locale: Include locales am gug st.
  * Fix class Desktop.web.iceweasel.locale: Include locale sr only since Stretch.
  * Fix class Desktop.web.iceweasel.locale: Stop include locales csb ku zu in Jessie (available only as security update).
  * Fix class Desktop.office.libreoffice: Stop include subclass theme.galaxy.avoid (not only since Stretch).
  * Fix class l10n.hunspell: Use hunspell (not myspell) for de-* only since Stretch.
  * Fix localeregion classes: Avoid packages hunspell-sr hunspell-vi in Jessie (clashes with myspell-da).

## [10.5.9] - 2016-05-31

### Changed

  * Fix avoid iceweasel locale packages de bg uk recommending only myspell since Stretch.

## [10.5.8] - 2016-05-30

### Changed

  * Fix work around myspell/hunspell clash for locales de-* gd uk since Stretch: Have classes l10n.hunspell.* include hunspell-* (not myspell-*).
  * Fix avoid Norwegian dictionaries missing since Stretch, use hunspell (not missing myspell) by default, and avoid mozilla locale packages recommending only myspell. See bug#790765, #802950.

## [10.5.7] - 2016-05-19

### Changed

  * Fix class l10n.hunspell.hunspell.lt: Package myspell-lt is gone (replaced with transitional package effectively conflicting with itself) since Stretch.
  * Fix class l10n.hunspell.hunspell.vi: Only hunspell exists.
  * Fix class Desktop.office.libreoffice.locale.ASIA: Drop locale package ku gone since Stretch.
  * Fix avoid Mozilla packages for locale lt since Stretch.

## [10.5.6] - 2016-05-19

### Changed

  * Fix class l10n.hunspell to favor myspell subclasses until hunspell accepted in Mozilla locale packages: bg ca cs de el es fr gd he hr hu it lt nb nl nn pl pt ru sk sl sv uk vi.

## [10.5.5] - 2016-05-18

### Changed

  * Fix class Desktop.email.icedove.bidi to include icedove-bidiui (not bogus icedove-l10n-bidiui).
  * Fix class Desktop.web.iceweasel.locale.AFRICA to stop include iceweasel-l10n-ak iceweasel-l10n-lg since Jessie.
  * Fix class Desktop.web.iceweasel.locale.ASIA to stop include iceweasel-l10n-ku since Stretch.
  * Fix class Desktop.web.iceweasel.locale.ZA to stop include iceweasel-l10n-zu since Stretch.
  * Fix class Desktop.web.iceweasel.locale.ASIA to consistently include iceweasel-l10n-ta (not iceweasel-l10n-ta-lk).
  * Fix stop include bogus mythes-zu.

## [10.5.4] - 2016-05-18

### Changed

  * Fix have class Desktop.office.libreoffice stop include subclass theme.galaxy.avoid since Stretch: Tango theme depends on Galaxy theme in LibreOffice 5). See bug#823962 and https://lists.debian.org/debian-openoffice/2015/09/msg00343.html
  * Extend class l10n.hunspell: Add be. Add br (since Jessie). Add bs (since Stretch).
  * Extend use of l10n/hunspell localeregion subclasses.
  * Extend use of l10n/hyphen localeregion subclasses.
  * Extend use of l10n/mythes localeregion subclasses.

## [10.5.3] - 2016-05-06

### Changed

  * Fix accidentally avoiding gnome-system-tools for stretch (leftover from buggy usermode).

## [10.5.2] - 2016-05-06

### Changed

  * Use https protocol in Vcs-Git URL.
  * Declare compliance with Debian Policy 3.9.8.
  * Modernize git-buildpackage config: Filter any .git* file.
  * Update copyright info: Extend copyright of packaging to cover current year.
  * Fix class Desktop.web.iceweasel.locale.INDIA.
  * Extend class Desktop.web.iceweasel.locale with locales an az gn ms uz (since stretch).
  * Fix class Desktop.web.iceweasel.locale.ALL: Include subclasses (not metapackage).
  * Extend class Desktop.web.iceweasel.locale with locale eo (since stretch).
  * Extend class Desktop.email.icedove.locale with localeregions AFRICA ALL.
  * Fix class Desktop.email.icedove.locale: Include in subclass ALL other subclasses (not metapackage).
  * Fix Desktop.scheduling.iceowl.locale: Extend subclass EU (not EUROPE) with subclass lt (since stretch).
  * Fix class Desktop.locale: Include in subclass AFRICA class Desktop.email.locale.AFRICA (jessie onwards, not only wheezy).
  * Extend class Desktop.locale: Extend subclasses AMERICAS AFRICA ASIA ZA with Desktop.scheduling.locale subclasses (since jessie).
  * Deprecate classes l10n.hunspell.nb.myspell l10n.hunspell.nn.myspell.
  * Extend/improve class l10n with subclasses AFRICA ALL ASIA ar ca da et is kn nb nn sv sw.
  * Fix class Desktop.office.libreoffice.locale: Include in subclass EUROPE (not EU) package libreoffice-l10n-ga.
  * Fix class Desktop.web.iceweasel.locale: Include in subclass ASIA (not EUROPE) package iceweasel-l10n-hy-am.
  * Fix class Desktop.scheduling.iceowl.locale: Include in subclass ASIA (not EUROPE) package iceowl-l10n-hy-am.
  * Fix class Desktop.email.icedoce.locale: Include in subclass ASIA (not EUROPE) package icedove-l10n-hy-am.
  * Extend/improve class l10n.aspell with subclasses EU bg cs el et fr hr hu it lt lv nl pl pt ro sk sl.
  * Extend/improve class l10n.hunspell subclasses ZA af.
  * Extend/improve class l10n.hunspell subclass en.
  * Extend/improve class l10n.hunspell subclasses EU bg cs el es et fr ga hr hu it lt lv nl pl pt ro sk sl.
  * Extend/improve class l10n.hyphen subclasses EU AUROPE cs es is ru.
  * Extend/improve class l10n.hunspell subclasses EUROPE an ca eu fo gd gl gv oc ru sr uk.
  * Extend/improve class l10n.hunspell subclasses NORDIC fo.
  * Extend/improve class l10n.aspell subclasses EUROPE br ca cy eu ga gl hsb ru uk.
  * Extend/improve class l10n.hunspell subclasses ASIA fa he hy kk kmr ko lo si th tl uz vi.
  * Fix class l10n.hunspell.sw.
  * Extend/improve class l10n.hunspell subclasses ALL AMERICAS eo pt.
  * Extend/improve class l10n.aspell subclasses AFRICA am.
  * Extend/improve class l10n.aspell subclasses ALL AMERICAS eo pt.
  * Extend/improve class l10n.aspell subclasses ASIA fa he hy kk kmr tl uz.
  * Extend/improve class l10n.hyphen subclasses INDIA ml or.
  * Extend/improve class l10n.enchant subclasses EU EUROPE ALL.
  * Extend/improve class l10n.hunspell subclasses ALL AFRICA AMERICAS EUROPE de en es fr.
  * Extend/improve class l10n.hyphen subclasses AFRICA ASIA as bn fr pa.
  * Extend/improve class l10n.ispell subclasses ALL AMERICAS AFRICA ASIA EUROPE EU da de en fi fo nb nn sv.
  * Extend/improve class l10n.ispell subclasses ALL AFRICAS AMERICAS ASIA EUROPE EU bg ca cs eo es et fr ga gd gl gv hu it nl pl pt ru tl uk.
  * Extend/improve class l10n.wordlist subclasses ALL AFRICAS AMERICAS EUROPE EU bg ca es fr gl it nl pl pt uk.
  * Extend/improve class l10n.hyphen subclasses ALL AFRICAS en.
  * Extend/improve class l10n.manpages subclasses ALL EUROPE EU de.
  * Extend/improve class l10n.mythes subclasses ALL AFRICA AMERICAS EUROPE EU bg ca es fr gl hu it lv pl pt ro ru sk sl uk.
  * Fix class l10n.mythes.en.AU.
  * Extend/improve class l10n/* subclasses AMERICAS en es fr pt.
  * Extend/improve class l10n.
  * Extend use of class l10n.

## [10.5.1] - 2016-05-04

### Changed

  * Fix avoid LibreOffice metapackage for localeregion ZA (individual locales explicitly included instead).
  * Add localeregion classes Desktop.scheduling.locale.ASIA Desktop.scheduling.iceowl.locale.ASIA (since jessie).
  * Add localeregion classes Desktop.scheduling.locale.AMERICAS Desktop.scheduling.iceowl.locale.AMERICAS (since jessie).
  * Extend class Desktop.scheduling.iceowl.locale with INDIA/ASIA locales ar bn he id pa ru si ta tr vi (since stretch).
  * Extend class Desktop.scheduling.iceowl.locale.AMERICAS with locale pt_BR (since stretch).
  * Extend class Desktop.scheduling.iceowl.locale with EU locales el fi hr pt-pt ro sl, and non-EU EUROPE locales be br cy gd gl hy-am lt nb-no rm sq tr uk (since stretch).

## [10.5.0] - 2016-05-03

### Changed

  * Add class Service.print.postscript.
  * Add class Console.editor.vim.minimal.
  * Add subclasses Hardware.net.bluetooth.acpi Hardware.laptop.acpi (to support non-ACPI hardware).
  * Add classes Desktop.media.video.totem Framework.media.gstreamer.gtk2.alsa Framework.media.gstreamer.gtk3.alsa.
  * Add class Framework.web.npapi.flash.
  * Add class Desktop.wm.fluxbox.
  * Fix have class Desktop.media.video.mpv.gtk include Desktop.media.video.mpv, and mark package mpv as auto-installed.
  * Add class Desktop.media.video.gtk.
  * Add classes Framework.theme.mate Desktop.environment.mate.file-manager Desktop.environment.mate.policykit-agent since Jessie.
  * Fix syntax error in class Desktop.filesystem.manager.xfce.
  * Fix double inclusion in class Desktop.environment.mate.official.
  * Fix add surf (not midori) in class Desktop.web.webkit.gtk: Midori is poorly maintained, and missing in Jessie.
  * Add class Service.hdaps, and include in Hardware.hdaps.
  * Include Pencil2D to class Desktop.design.animation.
  * Extend Hardware classes where relevant with hw.acpi hw.smp.

## [10.4.0] - 2015-12-23

### Changed

  * Fix avoid mail-transport-agent (not install gone lsb-invalid-mta) for Stretch.
  * Add classes Desktop.email.gtk Desktop.email.sylpheed.
  * Add class Desktop.media.video.mpv.gtk for Stretch.
  * Fix untie Desktop.environment.xfce.power from its superclass.
  * Fix avoid hpijs-ppds (not bogus hpijs) in class Service.print.cups.avoid.
  * Fix avoid only non-meta printer-driver-* depending on or recommending cups in class Service.print.cups.avoid.
  * Extend class Service.print with printer driver subclasses.
  * Add class Service.print.cups.ipp-usb for Stretch.
  * Add classes x11.gdm Desktop.filesystem.manager.nautilus Desktop.environment.gnome Framework.theme.gnome.
  * Add class Desktop.environment.lxde.policykit-agent.
  * Fix syntax of class Console.bidi.
  * Add class Desktop.office.libreoffice.gnome.
  * Add classes Service.print.cups.client Service.print.cups.bsd.
  * Extend class Service.print.minolta.zjstream with non-free firmware download tool hannah-foo2zjs.
  * Extend class Language.tcl.avoid to also avoid tk (useful with Service.print.minolta.zjstream).
  * Add class Service.print.minolta.zjstream.avoid.gui.
  * Add class Desktop.tools.print.
  * Stop include Console.filesystem.indexer in Console: Arguably too uncommon a feature.
  * Fix section label of class Framework.crypto.base.
  * Fix section label of class Framework.media.gstreamer.avoid.
  * Fix section label of class Framework.media.libvisual.limit.
  * Fix section label of class Framework.media.wildmidi.limit.
  * Fix section label of class Framework.notebook.base.
  * Fix section label of class Framework.media.gstreamer.avoid.
  * Add class Framework.documentation.
  * Fix add Console.locale.avoid, and stop avoid Desktop classes in other subclasses of Console.locale.
  * Fix typo in package description for class Desktop.wm.openbox.limit
  * Add class Framework.filesystem.gvfs.
  * Add classes Desktop.environment.lxde.limit Framework.auth.gnome.keyring.avoid.
  * Fix limit linux kernels marked as auto-installed.
  * rename class Desktop.tools.terminal.lxde → Desktop.terminal.lxde, and deprecate the former.
  * Subclass Console.tools. Update class Console.tools.filesystem to install package dfc in both stretch (i.e. only avoid in jessie).
  * Add class Framework.vcs.
  * Add notes on version numbering to README.source.

## [10.3.4] - 2015-12-04

### Changed

  * Include gnome-system-tools (not usermode - too unstable) in class Desktop.tools.account.
  * Add classes Desktop.environment.lxde Desktop.tools.terminal.lxde Framework.theme.lxde.

## [10.3.3] - 2015-11-28

### Changed

  * Extend class Service.ntp, and install openntpd (not ntp) by default.
  * Deprecate Hardware.crypto (as intended in release 7).
  * Add class Service.scheduler. Include in classes Service, and include Service.scheduler.anacron in Hardware.power.
  * Avoid task-* packages (use corresponding classes/packages instead.
  * Deprecate hw.power (as intended in release 7).
  * Add class Service.time.openntpd.force.
  * Add subclasses for Framework.pkg.apt and Admin.apt.
  * Fix have _backup() create *.bak file if *.orig exists, in class Admin.etc.functions.
  * Fix have _clone() force-copy (backup was made), in class Admin.etc.functions.
  * Fix have _clone() test existence of source before making backup, in class Admin.etc.functions.
  * Add function _backup_todir() to class Admin.etc.functions.
  * Add function _setline() to class Admin.etc.functions.
  * Add class Service.dns.caching.
  * Add class Console.setup.
  * Fix broken init class Framework.pkg.apt. Fix typo in class Framework.pkg.base.
  * Add class Service.firewall.
  * Add class Hardware.net.interfaces.
  * Add ARM-related subclasses for hw and Hardware.
  * Update TODO: Add note on regression testing shell code.
  * Add subclasses for Service.dhcp.
  * Add node gateway.

## [10.3.2] - 2015-11-23

### Changed

  * Fix include origami-pdf (not origami) in Desktop.design.pdf.

## [10.3.1] - 2015-11-21

### Changed

  * Pseudo-drop (i.e. empty) Stretch class Desktop.media.video.web: No longer sensible even to avoid.
  * Fix suppress failure to mark package auto-installed.

## [10.3.0] - 2015-11-07

### Changed

  * Add many more hyphen classes, and include them in libreoffice locale classes.
  * Tighten ZA localeregion classes to include only UK subsets of ispell and wordlist.
  * Fix include class l10n.hunspell.af in relevant locale classes.
  * Add class Console.bidi.

## [10.2.1] - 2015-11-03

### Changed

  * Fix add missing ZA localeregion classes, and class l10n.hyphen.en.
  * Add sample ShowMeBox nodes, to test classes Desktop.environment.gnustep Desktop.environment.razorqt Desktop.locale.ZA Framework.notebook.ipython.

## [10.2.0] - 2015-11-03

### Changed

  * Add Zulu locale classes.
  * Add Xhosa locale classes.
  * Add Northern Sotho locale classes.
  * Add Tswana locale classes.
  * Add Tsonga locale classes.
  * Add Swazi locale classes.
  * Add Venda locale classes.
  * Add Transvaal Ndebele locale classes.
  * Add Afrikaans locale classes.
  * Fix drop bogus package iceweasel-l10n-nso from class Desktop.web.iceweasel.locale.ZA.

## [10.1.1] - 2015-11-03

### Changed

  * Fix drop dfc from Wheezy and Jessie (not only Stretch).

## [10.1.0] - 2015-09-17

### Changed

  * Add class Framework.notebook.ipython.
  * Add classes Desktop.environment.gnustep Desktop.wm.windowmaker x11.wdm.
  * Extend class Framework.media.gstreamer.avoid to also avoid GStreamer 1.0 since Jessie.
  * Update class Console.tools to not include dfc, and Desktop.tools.account to not include usermode (both hopefully only temporarily).
  * Singlequote-escape \n.

## [10] - 2015-07-15

### Changed

  * Add class Desktop.photo.view.
  * Extend class Desktop.media.
  * Stop have class Desktop.media.video include EsounD wrapper (needed only for mplayer, not mplayer2).
  * Relax class Desktop.environment.xfce.audio to not include base Xfce, and add wrapper Desktop.media.audio.xfce.
  * Have Jessie class Desktop.media.audio.pulseaudio add pasystray.
  * Have class Hardware.laptop.lenovo.edge145 include Hardware.nic.realtek.
  * Enable firmware where relevant (i.e. all current motherboard classes).
  * Rename class Hardware.firmware → hw.firmware, deprecating the former.
  * Refactor class Desktop.environment.xfce to have less in base yet include more by default.

## [9] - 2015-07-14

### Changed

  * Add basic testsuite.
  * Fix stop use bogus class Desktop.office.libreoffice.java.avoid.
  * Extend coverage of Finnish locale.

## [8] - 2015-07-14

### Changed

  * Add Finnish spell engine Voikko to classes Desktop.office.libreoffice.locale.NORDIC and Desktop.mail.icedove.locale.NORDIC.
  * Fix refactor class Desktop.office.libreoffice to separate gtk support from avoiding some components, and avoid per default (accidentally messed up in release 7).
  * Fix add to Jessie class Desktop.office.libreoffice.locale.EUROPE package libreoffice-l10n-gd (accidentally dropped in release 7).
  * Fix add to Jessie class Desktop.office.libreoffice.locale.ASIA packages libreoffice-l10n-kk libreoffice-l10n-kmr (accidentally dropped in release 7).
  * Fix class l10n.hunspell.en.GB.myspell to use lowercase for package myspell-en-gb.

## [7] - 2015-07-07

### Changed

  * Update copyright info: Extend coverage for packaging to current year.
  * Drop (in debian/rules) jessie-deprecated class from before release of Jessie. Introduce stretch classes.
  * Add a bunch of TODOs.
  * Fix stop install package gstreamer0.10-ffmpeg from Jessie class Framework.media.gstreamer.avoid: Obsolete.
  * Fix stop avoid obsolete epdfview in Stretch class Desktop.office.gtk.document-viewer.
  * Fix rename Jessie class Desktop.office.pdf.gtk → Desktop.office.gtk.document-viewer.
  * Fix rename Jessie class Console.media.gstreamer.avoid → Framework.media.gstreamer.avoid.
  * Fix stop have class Task.harden.desktop include Framework.media.gstreamer.avoid: Unrelated to hardening, and clashes with indirect GStreamer dependency via task-xfce-desktop (see bug#774282).
  * Fix stop have Jessie class Desktop.scheduling.iceowl.locale.NORDIC include iceowl-l10n-fi: Only in experimental yet (see bug#774281).
  * Add Stretch node huayruro-classmate.
  * Add Stretch classes Desktop.environment.sugar Framework.theme.sugar.
  * Add Jessie node hans.
  * Add Jessie class Desktop.email.geary.
  * Add classes Desktop.editor.text.leafpad Desktop.environment.gnome.documentation.avoid Desktop.web.webkit.gnome Framework.color.colord.avoid Language.tcl.
  * Fix rename class Hardware(.init) → Hardware.base.
  * Fix have class Language.perl.base include Language.base (not bogus Language).
  * Add class Service.mail.mailx.avoid.
  * Add subclasses for Service.mail.mta.
  * Add subclasses for Hardware.gl.
  * Move class x11.xdg.icons → Framework.theme.tango, deprecating the former.
  * Fix have class Desktop.office include Desktop.office.gtk.document-viewer (not Desktop.office.gtk).
  * Fix have class Desktop.office.libreoffice include applications (not only GTK+ theming).
  * Stop have class Desktop.mobile include evolution-data-server.
  * Fix have class Hardware.laptop.intel.classmate include hw.x86.amd64 (not hw.mac.laptop).
  * Fix have class Hardware.motherboard.asus.a7n8x include hw.x86.amd64 hw.acpi (not bogus Hardware.cpu.amd.athlon).
  * Fix syntax of class Task.music.notation.musescore.
  * Fix have class Admin.apt include Framework.pkg.apt.cli.aptitude (not dropped Admin.apt.aptitude).
  * Simplify clean rules.
  * Add bug closures for classes l10n.hunspell.da.myspell l10n.myspell.de.CH.hunspell.
  * Add class Hardware.nic.broadcom.b43. Add class Hardware.nic.broadcom.b43 to class Hardware.laptop.lenovo.edge145 since Jessie: Non-free STA driver unneeded since kernel 4.0.5.
  * Add class Hardware.hdaps to classes Hardware.laptop.lenovo.edge145 Hardware.laptop.acer.aa0725.
  * Add a slew of Hardware subclasses and a few Service.display.xorg.video subclasses.
  * Have Stretch class Desktop.office.gtk.document-viewer install evince (replacing obsolete evince-gtk).
  * Add classes Desktop.office.gtk.spreadsheet Desktop.office.gtk.word-processor.
  * Redefine class Desktop.office.gtk to mean GTK+-related office tools (not mirror-class for GTK+ parts of default - i.e. LibreOffice - office tools). Rename and split class Desktop.office.pdf → Desktop.office.gtk.document-viewer Desktop.office.qt.pdf-viewer, deprecating the former.
  * Drop wheezy-deprecated and jessie-deprecated class from before release of Jessie. Introduce stretch classes.
  * Add class Framework.theme.gtk.gtk2.gnome.avoid.
  * Limit classes ...locale.SCANDINAVIA to mutual intelligible languages, and add superclass NORDIC for full coverage of the region.
  * Fix description for class Desktop.web.iceweasel.locale.SCANDINAVIA.
  * Avoid cufrequtils and deprecate (now empty) class hw.power: Better handled by kernel modules since Wheezy.
  * Adjust README: Classes are neither objective nor subjective.
  * Stop embed __PKGAUTOLIST__ in tweaks in class Admin.apt.auto.all (supported since Boxer 0.004).
  * Add APT-related subclasses Framework.pkg.*. Deprecate Admin.apt.aptitude.
  * Fix add init for class Desktop.web.webkit.
  * Add Jessie class Desktop.environment.mate.
  * Add class Desktop.web.webkit.qt.
  * Add class Desktop.office.pdf.qt.
  * Add class Desktop.wm.openbox and Jessie classes x11.lightdm.razorqt Desktop.environment.razorqt.
  * Rename Desktop.tools.gnupg → Desktop.tools.pgp.
  * Fix avoid Console.harden including Desktop.tools.gnupg.
  * Add classes Framework.crypto.openssh.pinentry.* and Desktop.tools.ssh.
  * Add class Task.music.notation.musescore.
  * Rename class Task.desktop.harden → Task.harden.desktop.
  * Have Desktop.design.pdf install origami.
  * Add class Framework.pkg.distinkt for Jessie, and include in Language.perl.devel.tools and (as class, not packages) Task.data.linkeddata.
  * Fix adapt and extend Framework.rdf and Task.data.linkeddata for Jessie.
  * Fix include only reverse dependencies of package libcode-tidyall-perl in Wheezy.
  * Add Service.filesystem.net.afp Service.filesystem.net.nfs Service.filesystem.net.smb.
  * Add Desktop.editor.rdf Framework.rdf Service.web.linkeddata Task.data.linkeddata.
  * Add Desktop.filesystem.indexer.semantic Service.filesystem.indexer.semantic Framework.media.libvisual.limit.
  * Add Console.harden Desktop.tools.gnupg Framework.crypto.gnupg.pinentry, and include Desktop.tools.gnupg in Desktop.harden.
  * Add Console.filesystem.indexer Desktop.filesystem.indexer Service.filesystem.indexer, and include locate-based indexer in base classes Console and Desktop.
  * Add Language.perl.
  * Fix consistently lowercase parameter labels.
  * Add Framework.web.webkit.webkit1.gtk.avoid.
  * Deprecate Desktop.filesharing.bittorrent. Rename Desktop.filesharing → Desktop.filesystem.net. Add Service.filesystem.net.bittorrent.
  * Rename Desktop.editor.* → Desktop.editor.text, deprecating Desktop.editor.mousepad and Desktop.editor.pyroom.
  * Fix include Console.locale.en (not deprecated Locale.english) in node lxp5.
  * Add Desktop.office.libreoffice.avoid, and include in Console.locale.de and Console.locale.en.
  * Enumerate bugs as separate parameters.
  * Fix use Hardware.net.bluetooth (not deprecated Hardware.bluetooth) in Hardware.laptop.
  * Update class Service.web.apache for Jessie: Install apache2 and mark apache2-mpm-worker as auto-installed.
  * Add class Service.database.
  * Refactor Service, adding base class and including classes Service.log Service.ssh Service.mail Service.time Service.web.
  * Refactor Service.mail, adding base class and including classes Service.mail.mta Service.mail.mda.
  * Slightly simplify Service.log.
  * Fix rename Service.dns.autoritatative → Service.dns.authoritatative, deprecating the former.
  * Refactor Service.web, adding base class and extending Service.web.uwsgi.
  * Add Framework.media base class.
  * Move base class Language → Language.base.
  * Add classes Desktop.web.locale.ZA Desktop.web.iceweasel.locale.ZA.
  * Deprecate Locale.english.
  * Add classes l10n.ispell.fo l10n.wordlist.fo, and include in l10n.fo.
  * Extend l10n.ispell.de classes to cover Swiss German.
  * Improve README: Clarify when appropriate to use pkg-avoid with lowercase classes.
  * Add English high-level locale classes.
  * Fix Desktop.office.libreoffice.locale.EU to include application-specific locale classes.
  * Add English low-level locale classes.
  * Add classes l10n.hunspell.de.medical l10n.wordlist.de.medical.
  * Fix include old German wordlist/ispell dictionary.
  * Fix list German country code classes in capital letters.
  * Improve l10n.hunspell.de to include all variants. Fix add l10n.de.
  * Drop l10n.myspell (folding it into l10n.hunspell).
  * Add class Console.editor.spelling.
  * Revert rename ...locale.ZA → ...locale.za: ZA is a country (not a locale).
  * Drop superfluously deprecated Desktop.scheduling.iceweasel.locale.danish for Jessie: No applications-specific locales were released.
  * Rename ....locale.ZA → ...locale.za (i.e. use lowercase locale - preserve uppercase for localeregions), deprecating the former.
  * Fix deprecate Desktop.locale.danish (was left overridden for Jessie).
  * Refactor German locale classes, including rename ...german → ...de, deprecating the former.
  * Fix typo.
  * Refactor danish locale classes, including rename ...danish → ...da, deprecating the former.
  * Have Desktop.locale.danish use explicit package dependencies (not task package - and also avoid -help package).
  * Add class Task.desktop. Deprecate class Blends.
  * Make node dharma generic (not Jessie-specific).
  * Add Desktop.locale.* localeregion classes.
  * Add localeregion SCANDINAVIA, and l10n classes for Faeroe (fo) and North Sami (se).
  * Fix Desktop.scheduling.iceowl.locale.SCANDINAVIA for Jessie: Drop locale nb-no still only in experimental.
  * Rename Console.media.midi.avoid → Framework.media.wildmidi.limit, deprecating the former.
  * Rename Console.media.gstreamer.avoid → Framework.media.gstreamer.avoid, deprecating the former.
  * Split Console.media.base from Console. Add Console.media.audio.alsa to Console.media.
  * Split Console.base from Console. Add Console.editor Console.filemanager Console.tools to Console.
  * Add misc. Bluetooth integration classes.
  * Rename Hardware.bluetooth → Hardware.net.bluetooth, deprecating the former. Deprecate hw.bluetooth.
  * Refactor Admin.tools.
  * Refactor and extend Console.multiplexer.
  * Refactor and extend Console.mail, deprecating Console.mail.alot.
  * Split class Console.editor.nano from Console.editor.
  * Add base class Task, and (Jessie-only) Task.data.transform.
  * Refactor class Hardware.harden: Include Hardware.net.pac.avoid, and split out Hardware.tools.harden.
  * Fix Desktop.design.web.sass Wheezy/Jessie difference.
  * Split class Admin.apt.tools into Admin.apt.tools.changes and Admin.apt.tools.bugs, and avoid the latter in base class (does not work well with approx, e.g. for use with live-build).
  * Fix Desktop.office.pdf.gtk for Jessie: Stop avoid epdfview.
  * Fix Console.media.gstreamer.avoid for Jessie: Stop avoid bluez-gstreamer.
  * Keep deprecated class files in separate directory.
  * Fix Desktop.scheduling.iceowl.locale for Jessie: Drop locales fi gd id lt nb-no pa-in sq tr uk still only in experimental.
  * Fix Desktop.scheduling.iceowl for Jessie.
  * Fix Service.print.cups.avoid Wheezy/Jessie difference.
  * Rename class Printing → Service.print, and refactor.
  * Split class Admin.auto into Admin.apt.auto.upgrade Admin.disk.auto Service.time. Include new Admin.apt.auto into Admin.auto.
  * Rename class Admin.lvm → Admin.disk.lvm.
  * Restructure class Admin: add Admin.apt, and include that and Admin.tools into base class.
  * Restructure class Admin: add base class.
  * Include Hardware.harden in each of Desktop.*.harden classes.
  * Rename class Desktop.crypto → Desktop.tools.harden, and add Desktop.harden.
  * Fix class Desktop.chat to include Desktop.chat.pidgin.
  * Rename class Desktop.chat.pidgin.crypto → Desktop.chat.pidgin.harden, and add Desktop.chat.harden.
  * Rename class Hardware.crypto → Hardware.harden.
  * Rename class Desktop.email.crypto → Desktop.email.harden.
  * Rename class Desktop.web.security → Desktop.web.harden (and fix move Jessie override below Desktop.web.iceweasel).
  * Restructure class Desktop.tools: Separate Desktop.tools.network and Desktop.tools.account.
  * Restructure class Desktop: Include Desktop.environment Desktop.email Desktop.editor Desktop.media Desktop.mobile Desktop.photo Desktop.office Desktop.scheduling Desktop.web.
  * Restructure class Desktop.environment: Include Desktop.screensaver.
  * Restructure class Desktop.editor: Include Desktop.editor.mousepad.
  * Restructure class Desktop.office: Include Desktop.office.gtk Desktop.office.word-processor Desktop.office.spreadsheet Desktop.office.presentation Desktop.office.pdf.
  * Really separate Desktop.office.libreoffice.
  * Restructure class Desktop: add base class.
  * Rename class Desktop.awesome → Desktop.environment.awesome.
  * Rename class Desktop.xfce → Desktop.environment.xfce.
  * Restructure class Desktop.web: Separate Desktop.web.iceweasel.
  * Extend class Desktop.screensaver: Include Desktop.screensaver.unicode, and add base class.
  * Restructure class Desktop.scheduling: Separate Desktop.scheduling.iceowl.
  * Extend class Desktop.photo: Include Desktop.photo.manage.gtk, and add base class.
  * Restructure class Desktop.office: Separate Desktop.office.libreoffice.
  * Fix extend class Desktop.office to include Desktop.office.gtk.
  * Fix add Desktop.office base class.
  * Extend class Desktop.office: Include Desktop.office.gtk, and add base class.
  * Fix really include Desktop.media.audio.alsa and Desktop.media.video in Desktop.media.
  * Split class Service.audio.pulseaudio into Service.audio.pulseaudio.client and Console.media.audio.pulseaudio, and include the latter in Service.audio.pulseaudio.
  * Extend Desktop.media.audio: add init, including Desktop.media.audio.alsa by default.
  * Extend class Desktop.media: Add base, and include Desktop.media.audio.alsa and Desktop.media.video by default.
  * Extend class Desktop.mobile.sync: Add base, and include sync by default.
  * Extend Desktop.filesharing: add base and init, including Desktop.filesharing.bittorrent by default.
  * Restructure Desktop.email: separate base from index, and add subclass Desktop.email.icedove.
  * Restructure Desktop.chat: separate base from index, and include Desktop.chat.pidgin.
  * Fix Printing class (and subclasses). Closes: bug#767360.
  * Fix adjust descriptions related to (but not really) tweaks.
  * Fix include Admin.etc.functions in Desktop.xfce.
  * Fix tweaks sometimes broken for classes Hardware.bluetooth and Hardware.power: Include Admin.etc.functions.
  * Add class Console.design.painting, and include in Console.design.
  * Add class Desktop.design.font, and include (except subclass Desktop.design.font.editor) in Desktop.design.
  * Add class Desktop.editor.kephra.
  * Add class Desktop.design.color, and include in Desktop.design.
  * Rename class Desktop.web.chromium → Desktop.web.blink.
  * Update to reclass 1.4 ABI for Desktop.design.painting.
  * Merge branch 'master-jessie'

## [6] - 2014-10-26

### Changed

  * Prepare for release: Update changelog.
  * Prepare for release: Update changelog.
  * Adapt to ABI of reclass 1.4: rename default class files index.yml → init.yml. See bug#766903.
  * New class Desktop.design.painting.kde.
  * New class Desktop.design.painting.gtk (included in its superclass).

## [5] - 2014-10-26

### Changed

  * Prepare for release: Update changelog.
  * New class Desktop.design.web. Exclude that and its subclass sass from Desktop.design.

## [4] - 2014-10-26

### Changed

  * Prepare for release: Update changelog.
  * Add classes Desktop.web.webkit and Deskto.web.chromium.
  * Restructure Desktop.web.* to separate base and gecko from index.
  * Add Jessie Desktop.media.video class to install mpv and avoid mplayer2.
  * Extend Jessie Desktop.web.security class to install xul-ext-y-u-no-validate.

## [3] - 2014-10-25

### Changed

  * Prepare for release: Update changelog.
  * Fix Console.design class.

## [2] - 2014-10-24

### Changed

  * Prepare for release: Update changelog.
  * Add design classes.
  * Rename class Desktop.mobile.photo → Desktop.photo.manage.gtk.

## [1] - 2014-10-20

### Added

  * Initial packaging.
  * Add Makefile to populate symlinks, and remove them on cleanup.
  * Have Admin.tools class install lsof.
  * Sort Desktop.web.security class.
  * Update Desktop.scheduling.locale.* to follow UN M.49 definition of Europe: Include Ukraine and Russia.
  * Fix package lists for iceowl locale classes.
  * Rename classes ...da → ...danish. Add and use more danish locale classes.
  * Derive class Console.tools for Jessie: include myrepos and auto-install mr.
  * Add a bunch of Jessie classes, and (preliminary) node dharma.
  * Add a bunch of classes, and node lxp5.
  * Fix missing colons. Thanks to Vasudev Kamath.
  * Rename classes ...l10n → ...locale.
  * Rename class Hardware.wifi → Hardware.nic.
  * Add laptop classes, and add node parl-greens.
  * Move mousepad to separate class Desktop.editor.mousepad.
  * Move aptitude to separate class Admin.apt.aptitude.
  * Move (without structurally renaming) class Hardware.laptop.
  * Simplify parl nodes, and add parl-desktop-india.
  * Improve and extend locale classes.
  * Extend and slightly rephrase README.
  * Rename wheezy class hw.acpi.base → hw.acpi.
  * Major rewrite: Use reclass.
  * Stop caring about libvisual-0.4-plugins being (auto-)installed: Not actually used.
  * Include core audio tools (no longer pulled in by GStreamer).
  * exclude webkit library (used only for maybe-risky JavaScript Proxy parsing).
  * Include generic volume control Volti. Exclude Xfce volumed and GStreamer framework.
  * Fix stop install iceweasel l10n n Croatian: Unavailable in Wheezy.
  * Fix exclude MIDI patches (bug#612509).
  * Extend desktop-mobile topic to include photo manager Shotwell.
  * Extend desktop topic to include mousepad.
  * Mark auto previously excluded recommendations evolution-data-server libvisual-0.4-plugins libwebkitgtk-1.0-0.
  * Fix exclude only software rasterizer: Include gvfs (fixing disk hot-plug detection) mplayer2 and libreoffice-ogltrans. Include recommendations evolution-data-server libvisual-0.4-plugins libwebkitgtk-1.0-0.
  * Fix exclude print support.
  * Explicitly include LibreOffice GTK+ style "tango".
  * Fix install icedove and iceweasel l10n for es-es, pt-pt and sv-se (not bogus es, pt or sv, nor missing lv).
  * Fix explicitly suppress OpenGL-depending libwebkitgtk-1.0-0 (pulled in by libproxy0 as alternative to no longer provided libmozjs10d.
  * Extend desktop topic to include ntp.
  * Explicitly exclude full libreoffice or Java-dependent parts.
  * Add new desktop topic media, including PulseAudio
  * Include screensaver unicode-screensaver.
  * Stop install non-free firmware-linux package (leave that to hardware-specific explicit instruction). Thanks to Paul Wise for reminder.
  * Exclude OpenGL and packages depending on it (until possible enabling only when supported by hardware).
  * Fix package exclusions: use dash suffix (not prefix).
  * Include power management tools.
  * Add Iceweasel plugins xul-ext-certificatepatrol xul-ext-refcontrol xul-ext-requestpolicy.
  * Introduce profile mobile, which includes SyncEvolution.
  * Distinguish desktop topics with localizations, and drop empty profile snippets for not yet available Iceowl locales.
  * Add Iceweasel plugin flashblock.
  * Introduce profile scheduling, which includes Icdove extension Iceowl.
  * Add sieve plugin for Icedove.
  * include gnome-disk-utility: Needed as GUI to change LUKS full-disk-encryption password.
  * Drop Breton from localeregion eu: Is a minority language (only official languages included for now).
  * Add localeregion eu (European Union).
  * Fix drop double locale.
  * Separate localeregions as own profiles (only 'all' for now).
  * Restructure to generate preseed files from skeleton + packagelists.

[Unreleased]: https://salsa.debian.org/boxer-team/box-data/compare/debian%2F10.9.12...master
[10.9.12]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.9.11...debian%2F10.9.12
[10.9.11]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.9.10...debian%2F10.9.11
[10.9.10]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.9.9...debian%2F10.9.10
[10.9.9]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.9.8...debian%2F10.9.9
[10.9.8]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.9.7...debian%2F10.9.8
[10.9.7]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.9.6...debian%2F10.9.7
[10.9.6]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.9.5...debian%2F10.9.6
[10.9.5]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.9.4...debian%2F10.9.5
[10.9.4]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.9.3...debian%2F10.9.4
[10.9.3]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.9.2...debian%2F10.9.3
[10.9.2]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.9.1...debian%2F10.9.2
[10.9.1]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.9.0...debian%2F10.9.1
[10.9.0]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.8.28...debian%2F10.9.0
[10.8.28]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.8.27...debian%2F10.8.28
[10.8.27]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.8.26...debian%2F10.8.27
[10.8.26]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.8.25...debian%2F10.8.26
[10.8.25]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.8.24...debian%2F10.8.25
[10.8.24]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.8.23...debian%2F10.8.24
[10.8.23]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.8.22...debian%2F10.8.23
[10.8.22]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.8.21...debian%2F10.8.22
[10.8.21]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.8.20...debian%2F10.8.21
[10.8.20]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.8.19...debian%2F10.8.20
[10.8.19]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.8.18...debian%2F10.8.19
[10.8.18]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.8.17...debian%2F10.8.18
[10.8.17]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.8.16...debian%2F10.8.17
[10.8.16]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.8.15...debian%2F10.8.16
[10.8.15]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.8.14...debian%2F10.8.15
[10.8.14]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.8.13...debian%2F10.8.14
[10.8.13]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.8.12...debian%2F10.8.13
[10.8.12]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.8.11...debian%2F10.8.12
[10.8.11]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.8.10...debian%2F10.8.11
[10.8.10]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.8.9...debian%2F10.8.10
[10.8.9]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.8.8...debian%2F10.8.9
[10.8.8]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.8.7...debian%2F10.8.8
[10.8.7]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.8.6...debian%2F10.8.7
[10.8.6]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.8.5...debian%2F10.8.6
[10.8.5]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.8.4...debian%2F10.8.5
[10.8.4]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.8.3...debian%2F10.8.4
[10.8.3]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.8.2...debian%2F10.8.3
[10.8.2]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.8.1...debian%2F10.8.2
[10.8.1]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.8.0...debian%2F10.8.1
[10.8.0]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.7.7...debian%2F10.8.0
[10.7.7]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.7.6...debian%2F10.7.7
[10.7.6]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.7.5...debian%2F10.7.6
[10.7.5]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.7.4...debian%2F10.7.5
[10.7.4]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.7.3...debian%2F10.7.4
[10.7.3]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.7.2...debian%2F10.7.3
[10.7.2]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.7.1...debian%2F10.7.2
[10.7.1]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.7.0...debian%2F10.7.1
[10.7.0]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.6.8...debian%2F10.7.0
[10.6.8]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.6.7...debian%2F10.6.8
[10.6.7]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.6.6...debian%2F10.6.7
[10.6.6]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.6.5...debian%2F10.6.6
[10.6.5]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.6.4...debian%2F10.6.5
[10.6.4]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.6.3...debian%2F10.6.4
[10.6.3]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.6.2...debian%2F10.6.3
[10.6.2]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.6.1...debian%2F10.6.2
[10.6.1]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.6.9...debian%2F10.6.1
[10.6.0]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.5.20...debian%2F10.6.0
[10.5.20]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.5.19...debian%2F10.5.20
[10.5.19]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.5.18...debian%2F10.5.19
[10.5.18]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.5.17...debian%2F10.5.18
[10.5.17]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.5.16...debian%2F10.5.17
[10.5.16]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.5.15...debian%2F10.5.16
[10.5.15]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.5.14...debian%2F10.5.15
[10.5.14]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.5.13...debian%2F10.5.14
[10.5.13]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.5.12...debian%2F10.5.13
[10.5.12]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.5.11...debian%2F10.5.12
[10.5.11]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.5.10...debian%2F10.5.11
[10.5.10]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.5.9...debian%2F10.5.10
[10.5.9]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.5.8...debian%2F10.5.9
[10.5.8]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.5.7...debian%2F10.5.8
[10.5.7]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.5.6...debian%2F10.5.7
[10.5.6]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.5.5...debian%2F10.5.6
[10.5.5]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.5.4...debian%2F10.5.5
[10.5.4]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.5.3...debian%2F10.5.4
[10.5.3]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.5.2...debian%2F10.5.3
[10.5.2]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.5.1...debian%2F10.5.2
[10.5.1]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.5.0...debian%2F10.5.1
[10.5.0]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.4.0...debian%2F10.5.0
[10.4.0]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.3.4...debian%2F10.4.0
[10.3.4]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.3.3...debian%2F10.3.4
[10.3.3]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.3.2...debian%2F10.3.3
[10.3.2]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.3.1...debian%2F10.3.2
[10.3.1]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.3.0...debian%2F10.3.1
[10.3.0]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.2.1...debian%2F10.3.0
[10.2.1]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.2.0...debian%2F10.2.1
[10.2.0]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.2.1...debian%2F10.2.0
[10.1.1]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10.1.0...debian%2F10.1.1
[10.1.0]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F10...debian%2F10.1.0
[10]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F9...debian%2F10
[9]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F8...debian%2F9
[8]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F7...debian%2F8
[7]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F6...debian%2F7
[6]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F5...debian%2F6
[5]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F4...debian%2F5
[4]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F3...debian%2F4
[3]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F2...debian%2F3
[2]: https://salsa.debian.org/boxer-team/boxer-data/compare/debian%2F1...debian%2F2
[1]: https://salsa.debian.org/boxer-team/boxer-data/commits/debian%2F1
