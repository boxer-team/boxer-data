#!/usr/bin/make -f

include /usr/share/dpkg/architecture.mk

# TODO: handle suites other than targeted host suite
NODES = $(wildcard /usr/share/boxer/trixie/nodes/*.yml)

contains_linux_amd64 = dharma hans huayruro-classmate parl-greens
contains_linux_armhf = gateway
contains_linux_i386 = lxp5
contains_falkon = showmebox-qt
contains_thunderbird = dharma parl-desktop-all parl-desktop-eu parl-desktop-india parl-desktop showmebox-gnustep

# TODO: avoid linux-*/falkon/thunderbird where unavailable
ifeq (,$(filter $(DEB_HOST_ARCH),amd64 arm64 armhf i386 mipsel))
NODES := $(filter-out $(foreach n,$(contains_falkon),%/$n.yml),$(NODES))
endif
ifeq (,$(filter $(DEB_HOST_ARCH),amd64))
NODES := $(filter-out $(foreach n,$(contains_linux_amd64),%/$n.yml),$(NODES))
endif
ifeq (,$(filter $(DEB_HOST_ARCH),armhf))
NODES := $(filter-out $(foreach n,$(contains_linux_armhf),%/$n.yml),$(NODES))
endif
ifeq (,$(filter $(DEB_HOST_ARCH),i386))
NODES := $(filter-out $(foreach n,$(contains_linux_i386),%/$n.yml),$(NODES))
endif
ifeq (,$(filter $(DEB_HOST_ARCH),amd64 arm64 i386 mips64el ppc64el ppc64 s390x))
NODES := $(filter-out $(foreach n,$(contains_thunderbird),%/$n.yml),$(NODES))
endif

all: $(NODES)

$(NODES):
	$(eval suite = /$(subst $() ,/,$(wordlist 1,4,$(subst /,$() ,$@))))
	$(eval node = $(basename $(notdir $@)))
	boxer compose --skel debian/tests/skel --datadir "$(suite)" "$(node)"
	bash ./script.sh
	rm preseed.cfg script.sh
